#thunder

*Note*: This is my work on a problem that Wingify gave me to solve before interview.

[Problem Statement](PROBLEM.md).

# Thought Process

I knew the web/api portion was gonna be easy for me; so instead I started focussing on the core problem: How do you do fast regex matching over a huge dataset?

I started looking for specific regex matching databases, and found an [interesting paper](http://oak.cs.ucla.edu/~cho/papers/cho-regex.pdf) on the topic. However it seemed to involve parsing out regex, and then using already-indexed data to make faster matches. This was a good approach, but I wasn't interested in delving too deep into regexes themselves (and instead focus on the architecture, which is what they asked me to do).

I realized that I'd need a proper dataset, so I went looking for one, and found the [httparchive datasets](http://httparchive.org/downloads.php). I didn't need the crawl data itself, just the urls, so I started fetching that.

The problem statement has a shortcut to performance: account ids. So, to simulate that I'll be using the domain name as the unique identifier for each account. I think thats a good enough approximation (how many accounts would be using multiple domains?).

Next, I decided I wanted to do migrations. Instead of just writing them out in plain SQL, I looked if I could do active-record style migrations without rails. Found a gem, and then wrote some migrations to create accounts and indexes.

I faced some issues with cleaning up the database (as original urls table used the url as the primary key), and that took a file fixing. Next, running the `rake create_accounts` command generated all the user accounts as per the domain information.

Accounts creation hung up on an invalid hostname (your_website_url_here.com.br), so I fixed that. The account creation was still very slow, so I started profiling it. I'd tried memoization, but it didn't help because there are very few urls per domain (495 accounts for every 500 urls).

# Setup

1. Make sure you have mysql installed
2. Create a `~/.my.cnf` file with your `root` credentials:  
    [client]
    user=root
    pass=root_password
3. Next, run `./bootstrap.sh` to download the dataset and import it to a database called `thunder`.