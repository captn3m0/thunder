Here is the problem statement:

Earlier we had released [a blog post][blog] on how for one of our projects we solved the problem of creating a system to store data coming in at a really high speed and perform search on the stored data. Our customers (enterprises, startups, e-commerce websites, banks, etc.) put our JavaScript library on their website. This library is served from homegrown CDN located in major geographical locations. So in a way all the traffic that comes to our customers’ websites ends up coming to our infrastructure as well. We also know which pages on our customers’ websites are requesting for our library.

Our customers want to know if a particular URL or a set of URLs matching a given regular expression have our JavaScript library on them. For this, we want to store all the unique URLs for every customer account that requested our JavaScript library.

We want to support checking of complete URLs and/or a set of URLs that match a provided regular expression. So sample request for checking if a particular URL ever requested for our library can be:

    {
    “account_id”: 123,
    “url”: “http://vwo.com/features”
    }

And the response to this can be:

    {
    “account_id”: 123,
    “url”: “http://vwo.com/features”,
    “found”: true
    }

The key found indicates if we found that URL in our system, indicating that our JavaScript library was requested by that page.

The user may provide a regular expression to get all the URLs that match the provided regex and have requested our library as well. The request for this can be something like:

    {
    “account_id”: 123,
    “pattern”: “http://vwo.com/blog/.*testing.*”
    }

And the response can be:

    {
    “matched_urls”: [
      “http://vwo.com/blog/ab-testing”,
      “http://vwo.com/blog/mvt-testing”,
      “http://vwo.com/blog/split-testing”
    ]
    }

The above response has an array of all the URLs that match the given regex and also requested for JavaScript library. If there are URLs on the customer’s website that match the provided regex but are not in the array of URLs in the response, then the reason for those URLs being missing can be the following:
the page at that URL has the JavaScript library installed on it, but the URL was not visited after the library was installed so a request for getting the library never reached our servers.
the URL does not have the JavaScript library installed on it.

The interface to communicate can be anything. For simplicity sake, stick to HTTP and JSON.

The constraints for the problem are:
We serve our JavaScript library at the rate of 10K requests per second.
We want this to be extremely fast as our frontend application’s performance depends on it. So our internal servers expect a response in the range of 10-100ms on private network (excluding network latency).
This is an important feature but dedicating a cluster of servers just to solve this will be an overkill. So we don’t really go beyond using one machine for this service (ignore problems like high availability for the scope of this question). Lets limit your machine

We have shared our solution (read [our blog post][blog]) which you are free to explore but your solution must be different than ours.

[blog]: http://engineering.wingify.com/fast-storage-with-rocksdb/