class AddIndexToAccounts < ActiveRecord::Migration
  def change
    add_index :accounts, :domain, unique: true, length: 300
  end
end
