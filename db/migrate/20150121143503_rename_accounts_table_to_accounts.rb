class RenameAccountsTableToAccounts < ActiveRecord::Migration
  def change
    rename_table :accounts_tables, :accounts
  end
end
