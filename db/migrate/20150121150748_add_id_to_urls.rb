class AddIdToUrls < ActiveRecord::Migration
  def change
    execute "ALTER TABLE urls DROP INDEX urlhash"
    execute "ALTER TABLE  `urls` ADD  `id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST"
  end
end
