class AddAccountIdToUrl < ActiveRecord::Migration
  def change
    add_column :urls, :account_id, :integer
  end
end
