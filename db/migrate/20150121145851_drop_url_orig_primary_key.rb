class DropUrlOrigPrimaryKey < ActiveRecord::Migration
  def change
    execute "ALTER TABLE urls DROP PRIMARY KEY"
  end
end
