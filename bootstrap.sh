#!/bin/sh
bundle install
rake db:setup
axel -n 10 -a http://httparchive.org/downloads/httparchive_urls.gz
gunzip httparchive_urls.gz
mv httparchive_urls data.sql
cat data.sql | mysql thunder
rake create_accounts