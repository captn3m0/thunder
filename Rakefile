require 'active_record_migrations'
require 'uri'

if GC.respond_to?(:copy_on_write_friendly=)
  GC.copy_on_write_friendly = true
end

ActiveRecordMigrations.configure do |c|
  # Other settings:
  c.schema_format = :sql # default is :ruby
  c.yaml_config = 'db/config.yml'
end
ActiveRecordMigrations.load_tasks

desc "Create accounts for all urls"
task :create_accounts => :environment do

  require_relative 'models/account'
  require_relative 'models/url'
  require 'digest/md5'

  CACHE = Hash.new
  jobs_per_process = 100
  process_count = 5

  Url.find_in_batches(:batch_size => jobs_per_process * process_count) do |group|
    batches = group.in_groups(process_count)

    batches.each do |batch|
      Process.fork do
        ActiveRecord::Base.establish_connection

        # Do the actual work
        batch.each do |url|
          begin
            uri = URI.parse(url.urlOrig)
          rescue URI::InvalidURIError => e
            puts "Invalid URL: #{url.urlOrig}"
            url.delete # Delete the invalid entry
            next # Go to the next url
          end
          domain = uri.host
          domain_hash = Digest::MD5.hexdigest(domain)
          if CACHE.has_key? domain_hash
            account_id = CACHE[domain_hash]
          else
            account_id = Account.find_or_create_by(domain: domain).id
            CACHE[domain_hash] = account_id
          end
          url.update_attribute(:account_id, account_id)
        end
      end
    end
    Process.waitall
  end
  puts "Accounts created"
end